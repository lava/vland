#! /usr/bin/python

#  Copyright 2014-2015 Linaro Limited
#
#  SPDX-License-Identifier: GPL-2.0-or-later
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

import logging
import sys
import re
import pexpect

if __name__ == '__main__':
    import os
    vlandpath = os.path.abspath(os.path.normpath(os.path.dirname(sys.argv[0])))
    sys.path.insert(0, vlandpath)
    sys.path.insert(0, "%s/.." % vlandpath)

from Vland.errors import InputError, PExpectError
from Vland.drivers.common import SwitchDriver, SwitchErrors

class TPLinkTLSG2XXX(SwitchDriver):

    connection = None
    _username = None
    _password = None
    _enable_password = None

    _capabilities = [
    ]

    # Regexp of expected hardware information - fail if we don't see
    # this
    _expected_descr_re = re.compile(r'TL-SG2\d\d\d|T1700')

    def __init__(self, switch_hostname, switch_telnetport=23, debug=False):
        SwitchDriver.__init__(self, switch_hostname, debug)
        self._systemdata = []
        self.exec_string = "/usr/bin/telnet %s %d" % (switch_hostname, switch_telnetport)
        self.errors = SwitchErrors()

    ################################
    ### Switch-level API functions
    ################################

    # Save the current running config into flash - we want config to
    # remain across reboots
    def switch_save_running_config(self):
        try:
            self._cli("copy running-config startup-config")
            self.connection.expect("OK|Success to save")
        except (PExpectError, pexpect.EOF):
            # recurse on error
            self._switch_connect()
            self.switch_save_running_config()

    # Restart the switch - we need to reload config to do a
    # roll-back. Do NOT save running-config first if the switch asks -
    # we're trying to dump recent changes, not save them.
    #
    # This will also implicitly cause a connection to be closed
    def switch_restart(self):
        self._cli("reboot")
        index = self.connection.expect(['Daving current', 'Continue?'])
        if index == 0:
            self._cli("n") # No, don't save
            self.connection.expect("Continue?")

        # Fall through
        self._cli("y") # Yes, continue to reset
        self.connection.close(True)

    # List the capabilities of the switch (and driver) - some things
    # make no sense to abstract. Returns a dict of strings, each one
    # describing an extra feature that that higher levels may care
    # about
    def switch_get_capabilities(self):
        return self._capabilities

    ################################
    ### VLAN API functions
    ################################

    # Create a VLAN with the specified tag
    def vlan_create(self, tag):
        logging.debug("Creating VLAN %d", tag)

        try:
            self._configure()
            self._cli("vlan %d" % tag)
            self._end_configure()

            # Validate it happened
            vlans = self.vlan_get_list()
            for vlan in vlans:
                if vlan == tag:
                    return
            raise IOError("Failed to create VLAN %d" % tag)

        except PExpectError:
            # recurse on error
            self._switch_connect()
            self.vlan_create(tag)

    # Destroy a VLAN with the specified tag
    def vlan_destroy(self, tag):
        logging.debug("Destroying VLAN %d", tag)

        try:
            self._configure()
            self._cli("no vlan %d" % tag)
            self._end_configure()

            # Validate it happened
            vlans = self.vlan_get_list()
            for vlan in vlans:
                if vlan == tag:
                    raise IOError("Failed to destroy VLAN %d" % tag)

        except PExpectError:
            # recurse on error
            self._switch_connect()
            self.vlan_destroy(tag)

    # Set the name of a VLAN
    def vlan_set_name(self, tag, name):
        logging.debug("Setting name of VLAN %d to %s", tag, name)

        try:
            self._configure()
            self._cli("vlan %d" % tag)
            self._cli("name %s" % name)
            self._end_configure()

            # Validate it happened
            read_name = self.vlan_get_name(tag)
            if read_name != name:
                raise IOError("Failed to set name for VLAN %d (name found is \"%s\", not \"%s\")"
                              % (tag, read_name, name))

        except (PExpectError, pexpect.EOF):
            # recurse on error
            self._switch_connect()
            self.vlan_set_name(tag, name)

    # Get a list of the VLAN tags currently registered on the switch
    def vlan_get_list(self):
        logging.debug("Grabbing list of VLANs")

        try:
            vlans = []
            regex = re.compile(r'^ *(\d+).*active')

            self._cli("show vlan brief")
            for line in self._read_long_output("show vlan brief"):
                match = regex.match(line)
                if match:
                    vlans.append(int(match.group(1)))
            return vlans

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self.vlan_get_list()

    # For a given VLAN tag, ask the switch what the associated name is
    def vlan_get_name(self, tag):
        logging.debug("Grabbing the name of VLAN %d", tag)

        try:
            name = None
            regex = re.compile(r'^ *\d+\s+(\S+).*(active)')
            self._cli("show vlan id %d" % tag)
            for line in self._read_long_output("show vlan id"):
                match = regex.match(line)
                if match:
                    name = match.group(1)
            name.strip()
            return name

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self.vlan_get_name(tag)

    ################################
    ### Port API functions
    ################################

    # Set the mode of a port: access or trunk
    def port_set_mode(self, port, mode):
        logging.debug("Setting port %s to %s", port, mode)
        if not self._is_port_mode_valid(mode):
            raise IndexError("Port mode %s is not allowed" % mode)
        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)
        # This switch does not support specific modes, so we can't
        # actually change the mode directly. However, we can and
        # should deal with the PVID and memberships of existing VLANs

        try:
            # We define a trunk to be on *all* VLANs on the switch in
            # tagged mode, and PVID should match the default VLAN (1).
            if mode == "trunk":
                # Disconnect all the untagged ports
                read_vlans = self._port_get_all_vlans(port, 'Untagged')
                for vlan in read_vlans:
                    self._port_remove_general_vlan(port, vlan)

                # And move to VLAN 1
                self.port_add_trunk_to_vlan(port, 1)
                self._set_pvid(port, 1)

            # And an access port should only be on one VLAN. Move to
            # VLAN 1, untagged, and set PVID there.
            if mode == "access":
                # Disconnect all the ports
                read_vlans = self._port_get_all_vlans(port, 'Untagged')
                for vlan in read_vlans:
                    self._port_remove_general_vlan(port, vlan)
                read_vlans = self._port_get_all_vlans(port, 'Tagged')
                for vlan in read_vlans:
                    self._port_remove_general_vlan(port, vlan)

                # And move to VLAN 1
                self.port_set_access_vlan(port, 1)
                self._set_pvid(port, 1)

            # Validate it happened
            read_mode = self._port_get_mode(port)
            if read_mode != mode:
                raise IOError("Failed to set mode for port %s" % port)

            # And cache the result
            self._port_modes[port] = mode

        except (PExpectError, pexpect.EOF):
            # recurse on error
            self._switch_connect()
            self.port_set_mode(port, mode)

    # Set an access port to be in a specified VLAN (tag)
    def port_set_access_vlan(self, port, tag):
        logging.debug("Setting access port %s to VLAN %d", port, tag)
        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)
        # Does the VLAN already exist?
        vlan_list = self.vlan_get_list()
        if not tag in vlan_list:
            raise IndexError("VLAN tag %d not recognised" % tag)

        try:
            # Add the new VLAN
            self._configure()
            self._cli("interface %s" % self._long_port_name(port))
            self._cli("switchport general allowed vlan %d untagged" % tag)
            self._cli("no shutdown")
            self._end_configure()

            self._set_pvid(port, tag)

            # Now drop all the other VLANs
            read_vlans = self._port_get_all_vlans(port, 'Untagged')
            for vlan in read_vlans:
                if vlan != tag:
                    self._port_remove_general_vlan(port, vlan)

            # Finally, validate things worked
            read_vlan = int(self.port_get_access_vlan(port))
            if read_vlan != tag:
                raise IOError("Failed to move access port %s to VLAN %d - got VLAN %d instead"
                              % (port, tag, read_vlan))

        except PExpectError:
            # recurse on error
            self._switch_connect()
            self.port_set_access_vlan(port, tag)

    # Add a trunk port to a specified VLAN (tag)
    def port_add_trunk_to_vlan(self, port, tag):
        logging.debug("Adding trunk port %s to VLAN %d", port, tag)
        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)
        try:
            self._configure()
            self._cli("interface %s" % self._long_port_name(port))
            self._cli("switchport general allowed vlan %d tagged" % tag)
            self._end_configure()

            # Validate it happened
            read_vlans = self.port_get_trunk_vlan_list(port)
            for vlan in read_vlans:
                if vlan == tag or vlan == "ALL":
                    return
            raise IOError("Failed to add trunk port %s to VLAN %d" % (port, tag))

        except PExpectError:
            # recurse on error
            self._switch_connect()
            self.port_add_trunk_to_vlan(port, tag)

    # Remove a trunk port from a specified VLAN (tag)
    def port_remove_trunk_from_vlan(self, port, tag):
        logging.debug("Removing trunk port %s from VLAN %d", port, tag)
        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)

        try:
            self._configure()
            self._cli("interface %s" % self._long_port_name(port))
            self._cli("no switchport general allowed vlan %d" % tag)
            self._end_configure()

            # Validate it happened
            read_vlans = self.port_get_trunk_vlan_list(port)
            for vlan in read_vlans:
                if vlan == tag:
                    raise IOError("Failed to remove trunk port %s from VLAN %d" % (port, tag))

        except PExpectError:
            # recurse on error
            self._switch_connect()
            self.port_remove_trunk_from_vlan(port, tag)

    # Get the configured VLAN tag for an access port (i.e. a port not
    # configured for tagged egress)
    def port_get_access_vlan(self, port):
        logging.debug("Getting VLAN for access port %s", port)
        vlan = 1
        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)
        regex = re.compile(r'(\d+)\s+.*Untagged')

        try:
            self._cli("show interface switchport %s" % self._long_port_name(port))
            for line in self._read_long_output("show interface switchport"):
                match = regex.match(line)
                if match:
                    vlan = match.group(1)
            return int(vlan)

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self.port_get_access_vlan(port)

    # Get the list of configured VLAN tags for a trunk port
    def port_get_trunk_vlan_list(self, port):
        logging.debug("Getting VLANs for trunk port %s", port)

        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)

        return self._port_get_all_vlans(port, 'Tagged')

    ################################
    ### Internal functions
    ################################

    # Connect to the switch and log in
    def _switch_connect(self):

        if not self.connection is None:
            self.connection.close(True)
            self.connection = None

        logging.debug("Connecting to Switch with: %s", self.exec_string)
        self.connection = pexpect.spawn(self.exec_string, logfile=self.logger)
        self._login()

        # No way to avoid paged output on this switch AFAICS

        # And grab details about the switch. in case we need it
        self._get_systemdata()

        # And also validate them - make sure we're driving a switch of
        # the correct model! Also store the serial number
        descr_regex = re.compile(r'Hardware Version\s+ - (.*)')
        descr = ""

        for line in self._systemdata:
            match = descr_regex.match(line)
            if match:
                descr = match.group(1)

        logging.debug("system description is %s", descr)

        if not self._expected_descr_re.match(descr):
            raise IOError("Switch %s not recognised by this driver: abort" % descr)

        # Now build a list of our ports, for later sanity checking
        self._ports = self._get_port_names()
        if len(self._ports) < 4:
            raise IOError("Not enough ports detected - problem!")

    def _login(self):
        logging.debug("attempting login with username \"%s\", password \"%s\", enable_password \"%s\"", self._username, self._password, self._enable_password)
        self.connection.expect('User Access Login')
        if self._username is not None:
            self.connection.expect("User:")
            self._cli("%s" % self._username)
        if self._password is not None:
            self.connection.expect("Password:")
            self._cli("%s" % self._password, False)
        while True:
            index = self.connection.expect(['User Name:', 'Password:', 'Bad passwords', 'authentication failed', r'\r\n(.*)(#|>)'])
            if index != 4: # Any other means: failed to log in!
                logging.error("Login failure: index %d\n", index)
                logging.error("Login failure: %s\n", self.connection.match.before)
                raise IOError

            # else - we're logged in ok
            # hit enter so we can grab the prompt reliably
            self._cli("")
            index = self.connection.expect([r'\r\n(.*)(#|>)'])
            self._prompt_name = re.escape(self.connection.match.group(1).strip())
            if self.connection.match.group(2) == ">":
                # Need to enter "enable" mode too
                self._cli("")
                self._cli("enable")
                if self._enable_password is not None and len(self._enable_password) > 0:
                    self.connection.expect("Password:")
                    self._cli("%s" % self._enable_password, False)
                    index = self.connection.expect(['Password:', 'Bad passwords', 'authentication failed', r'(.*)(#|>)'])
                    if index != 3: # Any other means: failed to log in!
                        logging.error("Enable password failure: %s\n", self.connection.match)
                        raise IOError
                return 0

    def _logout(self):
        logging.debug("Logging out")
        self._cli("exit", False)
        self.connection.close(True)

    def _configure(self):
        self._cli("configure")

    def _end_configure(self):
        self._cli("end")

    def _read_long_output(self, text):
        longbuf = []
        prompt = self._prompt_name + '#'
        while True:
            try:
                index = self.connection.expect(['Press any key to continue', prompt])
                if index == 0: # "Press any key to continue (Q to quit)"
                    for line in self.connection.before.split('\r\n'):
                        line1 = re.sub('\(Q to quit\).*\x0D ', '', line.strip())
#                        logging.debug("Appending line1 %s" % line1)
                        longbuf.append(line1)
                    self._cli(' ', False)
                elif index == 1: # Back to a prompt, says output is finished
                    break
            except (pexpect.EOF, pexpect.TIMEOUT):
                # Something went wrong; logout, log in and try again!
                logging.error("PEXPECT FAILURE, RECONNECT")
                self.errors.log_error_in(text)
                raise PExpectError("_read_long_output failed")
            except:
                logging.error("prompt is \"%s\"", prompt)
                raise

        for line in self.connection.before.split('\r\n'):
#            logging.debug("Seeing line %s" % line)
            line1 = re.sub('\(Q to quit\).*\x0D ', '', line.strip())
#            logging.debug("Appending line1 %s" % line1)
            longbuf.append(line1)

        return longbuf

    def _long_port_name(self, port):
        return re.sub('Gi', 'gigabitEthernet ', port)

    def _set_pvid(self, port, pvid):
        # Set a port's PVID
        self._configure()
        self._cli("interface %s" % self._long_port_name(port))
        self._cli("switchport pvid %d" % pvid)
        self._end_configure()

    def _port_get_all_vlans(self, port, port_type):
        vlans = []
        regex = re.compile(r'(\d+)\s+.*' + port_type)

        try:
            self._cli("show interface switchport %s" % self._long_port_name(port))
            for line in self._read_long_output("show interface switchport"):
                match = regex.match(line)
                if match:
                    vlan = match.group(1)
                    vlans.append(int(vlan))
            return vlans

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self._port_get_all_vlans(port, port_type)

    def _port_remove_general_vlan(self, port, tag):
        try:
            self._configure()
            self._cli("interface %s" % self._long_port_name(port))
            self._cli("no switchport general allowed vlan %d" % tag)
            self._end_configure()

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self._port_remove_general_vlan(port, tag)

    def _get_port_names(self):
        logging.debug("Grabbing list of ports")
        interfaces = []

        # Use "Link" to only identify lines in the output that match
        # interfaces that exist - it'll match "LinkUp" and "LinkDown"
        regex = re.compile(r'^\s*([a-zA-Z0-9_/]*).*Link')

        try:
            self._cli("show interface status")
            for line in self._read_long_output("show interface status"):
                match = regex.match(line)
                if match:
                    interface = match.group(1)
                    interfaces.append(interface)
                    self._port_numbers[interface] = len(interfaces)
            logging.debug("  found %d ports on the switch", len(interfaces))
            return interfaces

        except PExpectError:
            # recurse on error
            self._switch_connect()
            return self._get_port_names()

    # Get the mode of a port: access or trunk
    def _port_get_mode(self, port):
        logging.debug("Getting mode of port %s", port)

        if not self._is_port_name_valid(port):
            raise IndexError("Port name %s not recognised" % port)

        # This switch does not support specific modes, so we have to
        # make stuff up here. We define trunk ports to be on (1 or
        # many) tagged VLANs, anything not tagged to be access.
        read_vlans = self._port_get_all_vlans(port, 'Tagged')
        if len(read_vlans) > 0:
            return "trunk"
        else:
            return "access"

    def _show_config(self):
        logging.debug("Grabbing config")
        self._cli("show running-config")
        return self._read_long_output("show running-config")

    def _show_clock(self):
        logging.debug("Grabbing time")
        self._cli("show system-time")
        return self._read_long_output("show system-time")

    def _get_systemdata(self):
        logging.debug("Grabbing system sw and hw versions")

        self._cli("show system-info")
        self._systemdata = []
        for line in self._read_long_output("show system-info"):
            self._systemdata.append(line)

    # Wrapper around connection.send - by default, expect() the same
    # text we've sent, to remove it from the output from the
    # switch. For the few cases where we don't need that, override
    # this using echo=False.
    # Horrible, but seems to work.
    def _cli(self, text, echo=True):
        self.connection.send(text + '\r')
        if echo:
            self.connection.expect(text)

if __name__ == "__main__":

    # Simple test harness - exercise the main working functions above to verify
    # they work. This does *NOT* test really disruptive things like "save
    # running-config" and "reload" - test those by hand.

    import optparse

    switch = '10.172.2.50'
    username = 'admin'
    password = 'admin'
    enablepassword = None
    parser = optparse.OptionParser()
    parser.add_option("--switch",
                      dest = "switch",
                      action = "store",
                      nargs = 1,
                      type = "string",
                      help = "specify switch to connect to for testing",
                      metavar = "<switch>")
    parser.add_option("--username",
                      dest = "username",
                      action = "store",
                      nargs = 1,
                      type = "string",
                      help = "specify username",
                      metavar = "<username>")
    parser.add_option("--password",
                      dest = "password",
                      action = "store",
                      nargs = 1,
                      type = "string",
                      help = "specify password",
                      metavar = "<password>")
    parser.add_option("--enablepassword",
                      dest = "enablepassword",
                      action = "store",
                      nargs = 1,
                      type = "string",
                      help = "specify enablepassword",
                      metavar = "<enablepassword>")
    (opts, args) = parser.parse_args()
    if opts.switch:
        switch = opts.switch
    if opts.username:
        username = opts.username
    if opts.password:
        password = opts.password
    if opts.enablepassword:
        enablepassword = opts.enablepassword

    logging.basicConfig(level = logging.DEBUG,
                        format = '%(asctime)s %(levelname)-8s %(message)s')
    p = TPLinkTLSG2XXX(switch, 23, debug = False)
    p.switch_connect(username, password, enablepassword)

    print "Ports are:"
    buf = p.switch_get_port_names()
    p.dump_list(buf)

    print "VLANs are:"
    buf = p.vlan_get_list()
    p.dump_list(buf)

#    sys.exit(0)

    buf = p.vlan_get_name(2)
    print "VLAN 2 is named \"%s\"" % buf

    print "Create VLAN 3"
    p.vlan_create(3)

    buf = p.vlan_get_name(3)
    print "VLAN 3 is named \"%s\"" % buf

    print "Set name of VLAN 3 to test333"
    p.vlan_set_name(3, "test333")

    buf = p.vlan_get_name(3)
    print "VLAN 3 is named \"%s\"" % buf

    print "VLANs are:"
    buf = p.vlan_get_list()
    p.dump_list(buf)

    print "Destroy VLAN 3"
    p.vlan_destroy(3)

    print "VLANs are:"
    buf = p.vlan_get_list()
    p.dump_list(buf)

    buf = p.port_get_mode("Gi1/0/10")
    print "Port Gi1/0/10 is in %s mode" % buf

    buf = p.port_get_mode("Gi1/0/11")
    print "Port Gi1/0/11 is in %s mode" % buf

    # Test access stuff
    buf = p.port_get_mode("Gi1/0/9")
    print "Port Gi1/0/9 is in %s mode" % buf

    print "Set Gi1/0/9 to access mode"
    p.port_set_mode("Gi1/0/9", "access")

    print "Move Gi1/0/9 to VLAN 4"
    p.port_set_access_vlan("Gi1/0/9", 4)

    buf = p.port_get_access_vlan("Gi1/0/9")
    print "Read from switch: Gi1/0/9 is on VLAN %s" % buf

    print "Move Gi1/0/9 back to VLAN 1"
    p.port_set_access_vlan("Gi1/0/9", 1)

    # Test access stuff
    print "Set Gi1/0/9 to trunk mode"
    p.port_set_mode("Gi1/0/9", "trunk")
    print "Read from switch: which VLANs is Gi1/0/9 on?"
    buf = p.port_get_trunk_vlan_list("Gi1/0/9")
    p.dump_list(buf)
    print "Add Gi1/0/9 to VLAN 2"
    p.port_add_trunk_to_vlan("Gi1/0/9", 2)
    print "Add Gi1/0/9 to VLAN 3"
    p.port_add_trunk_to_vlan("Gi1/0/9", 3)
    print "Add Gi1/0/9 to VLAN 4"
    p.port_add_trunk_to_vlan("Gi1/0/9", 4)
    print "Read from switch: which VLANs is Gi1/0/9 on?"
    buf = p.port_get_trunk_vlan_list("Gi1/0/9")
    p.dump_list(buf)

    p.port_remove_trunk_from_vlan("Gi1/0/9", 3)
    p.port_remove_trunk_from_vlan("Gi1/0/9", 3)
    p.port_remove_trunk_from_vlan("Gi1/0/9", 4)
    print "Read from switch: which VLANs is Gi1/0/9 on?"
    buf = p.port_get_trunk_vlan_list("Gi1/0/9")
    p.dump_list(buf)


    print "Saving config"
    p.switch_save_running_config()

    p.switch_disconnect()
#    p._show_config()
