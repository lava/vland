#  Copyright 2014-2016 Linaro Limited
#
#  SPDX-License-Identifier: GPL-2.0-or-later
#
#  Authors: Dave Pigott <dave.pigot@linaro.org>,
#           Steve McIntyre <steve.mcintyre@linaro.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.

class VlandError(Exception):
    """
    Base exception and error class for the vlan daemon
    """


class CriticalError(VlandError):
    """
    The critical error
    """

class NotFoundError(VlandError):
    """
    Couldn't find object
    """

class InputError(VlandError):
    """
    Invalid input
    """

class ConfigError(VlandError):
    """
    Invalid configuration
    """

class SocketError(VlandError):
    """
    Socket connection failure
    """

class PExpectError(VlandError):
    """
    CLI communication failure
    """

class Error:
    OK = 0
    FAILED = 1
    NOTFOUND = 2
