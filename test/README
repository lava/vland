Testing overview
================

VLANd is not a trivial thing to test - it needs a fair bit of hardware
connected together to be able to verify correct functionality

The tests I have set up here involve using 5 test switches, each with
2 test machines attached. This is the configuration we have in the
Linaro test lab:

                                   +-------------------+
                                   |    vlandserver    |
                                   +---------+---------+
                                             |
                                             |
                                             |
                                             |Gi1/0/1
           +---------------------------------------------------------------+
           |                        vlandswitch01                          |
           ++---------+---------+----------+----------+----------+---------+
            |Gi1/0/2  |Gi1/0/3  |Gi1/0/23  |Gi1/0/24  |Gi1/0/20  |Gi1/0/19
+----------+|         |         |          |          |          |
|panda01   ++         |         |          |          |          |
+----------+          |         |          |          |          |
+----------+          |         |          |          |          |
|arndale01 +----------+         |          |          |          |
+----------+                    |          |          |          |
                                |          |          |          |
          +---------------------+          |          |          |
          |                   +------------+      +---+          +----+
          |gi4                |gi52               |Gi1/0/1            |1/0/24
  +-------+-------+   +-------+-------+   +-------+-------+   +-------+-------+
  | vlandswitch02 |   | vlandswitch03 |   | vlandswitch04 |   + vlandswitch05 |
  +-+--------+----+   +-+--------+----+   +-+--------+----+   +-+--------+----+
    |fa2     |fa25      |gi2     |gi25      |Gi1/0/2 |Gi1/0/3   |1/0/21  |1/0/22
   +++      +++        +++      +++        +++      +++        +++      +++
   |a|      |p|        |a|      |p|        |i|      |a|        |i|      |i|
   |r|      |a|        |r|      |a|        |m|      |r|        |m|      |m|
   |n|      |d|        |n|      |n|        |x|      |n|        |x|      |x|
   |d|      |n|        |d|      |d|        |5|      |d|        |5|      |5|
   |a|      |a|        |a|      |a|        |3|      |a|        |3|      |3|
   |l|      |0|        |l|      |0|        |0|      |l|        |0|      |0|
   |e|      |2|        |e|      |3|        |3|      |e|        |1|      |2|
   |0|      | |        |0|      | |        | |      |0|        | |      | |
   |2|      | |        |3|      | |        | |      |4|        | |      | |
   +-+      +-+        +-+      +-+        +-+      +-+        +-+      +-+


vlandserver01 is the machine where VLANd is running, and also where
the tests are controlled. It has an extra ethernet interface to talk
to the outside world, with *no* forwarding enabled. This is to make
sure the test network here is fully isolated.

The switches in the test network are deliberately varied models
covering the switches that we currently have drivers
for:

vlandswitch01 is a 24-port Cisco Catalyst 3750 (mainly 1G ports with 10G uplinks)
vlandswitch02 is a 48-port Cisco SF300         (all 1G ports)
vlandswitch03 is a 52-Port Cisco SG300         (mainly 100M ports with 1G uplinks)
vlandswitch04 is a 16-Port TP-Link TL-SG2216   (all 1G ports)
vlandswitch03 is a 24-port Netgear XSM 7224S   (all 10G ports)

The test machines are a set of old/spare machines that we had in the
Linaro lab: 3 TI Panda boards, 4 Samsung Arndale boards and 3
Freescale iMX53 Quickstart boards. The ordering and the machine type
doesn't matter here at all - we just want a set of machines with
onboard ethernet that we can run trivial (ping and report) scripts on.

Initial test coverage
=====================

I'm starting off with a simple set of tests that can be cycled
through. Each is of the style:

 * START
 * Ensure all the ports we're using are on their base VLANs
 * Start all the test machines logging (and wait 60s)
 * Create some VLANs
 * Wait for everything to settle
 * Move some of the ports to those new VLANs
 * Wait for everything to settle
 * Move things back to their base VLANs
 * Wait for everything to settle
 * Stop all the test machines logging (and wait 60s)
 * Grab all the logs and store them as test results
 * END
 * Parse the results and say PASS/FAIL

 As each test should put things back into a known sane state, they can
 be chained together easily.
