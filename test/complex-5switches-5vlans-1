#!/bin/bash
#
# More complex VLANd test script

set -e

NAME="complex-5switches-5vlans-1"

DESCRIPTION="Check VLAN isolation for 10 machines in 5 VLANs scattered across multiple switches."

# List all the switches and hosts we need to use, if not using all of
# them. We can make tests run faster by not involving *all* of them in
# every test.
HOSTS="imx5301 imx5302 imx5303"
HOSTS="$HOSTS panda01 panda02 panda03"
HOSTS="$HOSTS arndale01 arndale02 arndale03 arndale04"

#SWITCHES="vlandswitch05"   # Let the system work things out

# Show more detail during test output
VERBOSE=0

# And give a filename for logging
LOGFILE=$0-$$.log

# Include the core test wrapper code that makes life easier
DIR=$(dirname $0)
. ${DIR}/test-common

# Ensure all the ports we're using are on their base VLANs
log "checking base VLANs"
verify_all_hosts_are_base
log "$HOSTS are all on their base VLANs - good"

# Clear old logfiles from our test machines
stop_logging
clear_logs

# Start all the test machines logging, then wait 60s to let all of
# them show baseline results before we start testing
start_logging
log "CHECK INIT START"
log "CHECK INIT CHECK VLAN_BASE:imx5301:imx5302:imx5303:panda01:panda02:panda03:arndale01:arndale02:arndale03:arndale04"
pause 60
log "CHECK INIT END"

# Create 2 VLANs: tag 30, named "test30" and tag 31, named "test31"
log "Creating new VLAN tag 30"
OUTPUT=$(run_admin_command create_vlan --name test30 --tag 30 --is_base_vlan false)
VLAN_ID1=$(run_admin_command lookup_vlan_by_tag --tag 30)
log "Creating new VLAN tag 31"
OUTPUT=$(run_admin_command create_vlan --name test31 --tag 31 --is_base_vlan false)
VLAN_ID2=$(run_admin_command lookup_vlan_by_tag --tag 31)
log "Creating new VLAN tag 32"
OUTPUT=$(run_admin_command create_vlan --name test32 --tag 32 --is_base_vlan false)
VLAN_ID3=$(run_admin_command lookup_vlan_by_tag --tag 32)
log "Creating new VLAN tag 33"
OUTPUT=$(run_admin_command create_vlan --name test33 --tag 33 --is_base_vlan false)
VLAN_ID4=$(run_admin_command lookup_vlan_by_tag --tag 33)
log "Creating new VLAN tag 34"
OUTPUT=$(run_admin_command create_vlan --name test34 --tag 34 --is_base_vlan false)
VLAN_ID5=$(run_admin_command lookup_vlan_by_tag --tag 34)

log "Created new VLANs with IDs $VLAN_ID1, $VLAN_ID2, $VLAN_ID3, $VLAN_ID4 and $VLAN_ID5"

# Wait 10s for everything to settle
pause 10

# Move some of the test machines to these new VLANs, pausing at each
# setup
log "Moving panda01 to VLAN ID $VLAN_ID1"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${panda01_PORT_ID} --vlan_id $VLAN_ID1)
log "Moving panda02 to VLAN ID $VLAN_ID1"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${panda02_PORT_ID} --vlan_id $VLAN_ID1)
log "Done moving ports to VLAN ID $VLAN_ID1"
pause 60
log "CHECK STEP1 START"
log "CHECK STEP1 CHECK VLAN_30:panda01:panda02"
log "CHECK STEP1 CHECK VLAN_BASE:imx5301:imx5302:imx5303:panda03:arndale01:arndale02:arndale03:arndale04"
pause 60
log "CHECK STEP1 END"

log "Moving panda03 to VLAN ID $VLAN_ID2"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${panda03_PORT_ID} --vlan_id $VLAN_ID2)
log "Moving imx5303 to VLAN ID $VLAN_ID2"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${imx5303_PORT_ID} --vlan_id $VLAN_ID2)
log "Done moving ports to VLAN ID $VLAN_ID2"
pause 60
log "CHECK STEP2 START"
log "CHECK STEP2 CHECK VLAN_30:panda01:panda02"
log "CHECK STEP2 CHECK VLAN_31:panda03:imx5303"
log "CHECK STEP2 CHECK VLAN_BASE:imx5301:imx5302:arndale01:arndale02:arndale03:arndale04"
pause 60
log "CHECK STEP2 END"

log "Moving imx5301 to VLAN ID $VLAN_ID3"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${imx5301_PORT_ID} --vlan_id $VLAN_ID3)
log "Moving arndale03 to VLAN ID $VLAN_ID3"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale03_PORT_ID} --vlan_id $VLAN_ID3)
log "Done moving ports to VLAN ID $VLAN_ID3"
pause 60
log "CHECK STEP3 START"
log "CHECK STEP3 CHECK VLAN_30:panda01:panda02"
log "CHECK STEP3 CHECK VLAN_31:panda03:imx5303"
log "CHECK STEP3 CHECK VLAN_32:imx5301:arndale03"
log "CHECK STEP3 CHECK VLAN_BASE:imx5302:arndale01:arndale02:arndale04"
pause 60
log "CHECK STEP3 END"

log "Moving arndale01 to VLAN ID $VLAN_ID4"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale01_PORT_ID} --vlan_id $VLAN_ID4)
log "Moving arndale04 to VLAN ID $VLAN_ID4"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale04_PORT_ID} --vlan_id $VLAN_ID4)
log "Done moving ports to VLAN ID $VLAN_ID4"
pause 60
log "CHECK STEP4 START"
log "CHECK STEP4 CHECK VLAN_30:panda01:panda02"
log "CHECK STEP4 CHECK VLAN_31:panda03:imx5303"
log "CHECK STEP4 CHECK VLAN_32:imx5301:arndale03"
log "CHECK STEP4 CHECK VLAN_33:arndale01:arndale04"
log "CHECK STEP4 CHECK VLAN_BASE:imx5302:arndale02"
pause 60
log "CHECK STEP4 END"

log "Moving imx5302 to VLAN ID $VLAN_ID5"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${imx5302_PORT_ID} --vlan_id $VLAN_ID5)
log "Moving arndale02 to VLAN ID $VLAN_ID5"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale02_PORT_ID} --vlan_id $VLAN_ID5)
log "Done moving ports to VLAN ID $VLAN_ID4"
pause 60
log "CHECK STEP5 START"
log "CHECK STEP5 CHECK VLAN_30:panda01:panda02"
log "CHECK STEP5 CHECK VLAN_31:panda03:imx5303"
log "CHECK STEP5 CHECK VLAN_32:imx5301:arndale03"
log "CHECK STEP5 CHECK VLAN_33:arndale01:arndale04"
log "CHECK STEP5 CHECK VLAN_34:imx5302:arndale02"
pause 60
log "CHECK STEP5 END"


# Move test machines back to their base VLANs
log "Moving ports back to base"
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${imx5301_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${imx5302_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${imx5303_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${panda01_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${panda02_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${panda03_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale01_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale02_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale03_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale04_PORT_ID})
log "Done moving ports back to base"

# Wait 60s for everything to settle
pause 60

log "CHECK FINI START"
log "CHECK FINI CHECK VLAN_BASE:imx5301:imx5302:imx5303:panda01:panda02:panda03:arndale01:arndale02:arndale03:arndale04"
pause 60
log "CHECK FINI END"

# Check that they're all back on their base VLANs
log "Checking base VLANs after the test"
verify_all_hosts_are_base

log "Delete the test VLANs"
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID1})
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID2})
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID3})
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID4})
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID5})

# Stop all the test machines logging (and wait 60s)
stop_logging
pause 20

log "Test done, grab logs"
# Grab logs from the machines
grab_logs >> $LOGFILE

# Clear old logs
clear_logs

# How did the test do?
check_test_steps

# DONE!
