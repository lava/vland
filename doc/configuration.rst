 .. index:: configuration, vland.cfg

.. _vland_cfg:

Configuration
#############

VLANd uses a simple ini-style file format for configuration. The
default location for the file is ``/etc/vland.cfg``. As it contains
passwords for database and switch access, it should be kept private
(i.e. not world-readable). The example config file with this package
(reproduced below) includes commented examples for everything than
VLANd might use.

There are several groups of things that should be configured
here. VLANd itself uses all the settings (database, port numbers,
switch config and login details). The admin program ``vland-admin``
also needs to know the port number for VLANd to be able to
communicate. Obviously, if you choose a different communication port
then all API users will need to be updated too.

.. literalinclude:: ../vland.cfg
