 .. index:: visualisation, web ui, ui

.. _visualisation_interface:

Visualisation
#############

VLANd comes with a simple web-based visualisation interface which can
be a very useful aid in understanding switch and port setup. Simply
hover over a port to see more details about it, as shown here.

The visualisation interface is disabled by default, but it's easy to
turn on in :ref:`vland.cfg <vland_cfg>`. Then point your browser at
the correct port (e.g. http://localhost:3081).

.. image:: images/vis1.png

